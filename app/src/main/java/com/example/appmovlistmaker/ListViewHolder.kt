package com.example.appmovlistmaker

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val ListItemId = itemView.findViewById<TextView>(R.id.item_id)
    val listItemTitle = itemView.findViewById<TextView>(R.id.item_title)
}
