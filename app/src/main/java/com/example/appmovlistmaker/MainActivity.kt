package com.example.appmovlistmaker

import android.content.Intent
import android.os.Bundle
import android.text.InputType
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), ListSelectionListener {

    private lateinit var listsRecyclerView: RecyclerView
    val listDataManager: ListDataManager = ListDataManager(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val lists = listDataManager.readLists()

        listsRecyclerView = findViewById(R.id.lists_recycler_view)
        listsRecyclerView.layoutManager = LinearLayoutManager(this)
        listsRecyclerView.adapter = ListRecyclerViewAdapter(lists, this)

        fab.setOnClickListener { view ->
            showDialog()
        }
    }

    private fun showDialog(){
        var builder  = AlertDialog.Builder(this)
        var listTitledEditText = EditText(this)
        listTitledEditText.inputType = InputType.TYPE_CLASS_TEXT

        builder.setTitle(getString(R.string.enter_list_name))
        builder.setView(listTitledEditText)

        builder.setPositiveButton(getString(R.string.add_list)) { dialog, _ ->
            val list = TaskList(listTitledEditText.text.toString())
            listDataManager.saveList(list)

            val adapter = listsRecyclerView.adapter as ListRecyclerViewAdapter
            adapter.addList(list)

            dialog.dismiss()
            showListDetails(list)
        }

        builder.setNegativeButton(getString(R.string.cancel)) { dialog, _ ->
            dialog.dismiss()
        }

        builder.create().show()
    }

    private fun showListDetails(list: TaskList) {
        val listDetailIntent = Intent(this, ListDetailActivity::class.java)
        listDetailIntent.putExtra(INTENT_LIST_KEY, list)
        startActivity(listDetailIntent)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    companion object {
        const val INTENT_LIST_KEY = "list"
    }

    override fun listItemSelected(list: TaskList) {
        showListDetails(list)
    }
}
